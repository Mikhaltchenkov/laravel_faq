<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable = ['name', 'created_by', 'updated_at', 'created_at'];

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function questionsPublished()
    {
        return $this->questions()->where('state', 2);
    }

    public function questionsAwaiting()
    {
        return $this->questions()->where('state', 1);
    }
}
