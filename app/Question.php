<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['question', 'answer', 'people_id', 'state', 'thread_id', 'answered_by'];

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function people()
    {
        return $this->belongsTo(People::class);
    }

    public function answered()
    {
        return $this->belongsTo(User::class, 'answered_by');
    }

}
