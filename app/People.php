<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $fillable = ['name', 'email', 'is_banned'];

    protected $table = 'peoples';
}
