<?php

namespace App\Http\Controllers;

use App\People;
use App\Question;
use App\Thread;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $threads = Thread::all();
        return view('front.index', ['threads' => $threads]);
    }

    public function create()
    {
        $threads = Thread::all();
        return view('front.create', ['threads' => $threads]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:64',
            'email' => 'required|email',
            'question' => 'required',
        ]);

        $people = People::all()->where('email', $request->email)->first();

        if (empty($people)) {
            $people = People::create(['name' => $request->name, 'email' => $request->email]);
        }

        $attributes = $request->all();
        $attributes['people_id'] = $people->id;


        Question::create($attributes);

        return redirect()->route('front.index')->with('message', 'Ваш вопрос направлен администратору.');
    }
}
