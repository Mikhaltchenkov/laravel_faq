<?php

namespace App\Http\Controllers;

use App\People;
use App\Question;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $questions = Question::all();
        if (!empty($request->thread_id)) {
            $questions = $questions->where('thread_id', $request->thread_id);
        }
        if (!empty($request->state)) {
            $questions = $questions->where('state', $request->state);
        }
        $threads = Thread::all();
        return view('questions.index', ['questions' => $questions, 'threads' => $threads, 'thread_id' => $request->thread_id, 'state' => $request->state]);
    }

    public function destroy($id)
    {
        $question = Question::find($id);
        $question->delete();

        return back()->with('message', 'Вопрос успешно удален.');
    }

    public function edit($id)
    {
        $question = Question::find($id);
        $threads = Thread::all();
        $peoples = People::all();
        return view('questions.edit', ['question' => $question, 'threads' => $threads, 'peoples' => $peoples]);
    }

    public function unpublish(Request $request, $id)
    {
        $question = Question::find($id);
        $question->state = 3;
        $question->save();
        return redirect()->route('questions')->with('message', 'Вопрос успешно скрыт.');
    }

    public function publish(Request $request, $id)
    {
        $question = Question::find($id);
        if (empty($question->answer)) {
            return redirect()->route('questions')->with('message', 'На вопрос не дан ответ, публикация не возможна.');
        }
        $question->state = 2;
        $question->save();
        return redirect()->route('questions')->with('message', 'Вопрос успешно опубликован.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'question' => 'required|max:128',
        ]);

        $question = Question::find($id);
        $old_answer = $question->answer;
        $question->update($request->all());

        if (!empty($request->answer)) {
            if (strcmp($request->answer, $old_answer) !== 0) {
                $question->answered_by = Auth::user()->id;
                if ($question->state == 1) {
                    $question->state = 2;
                }
            }
        } else {
            $question->state = 1;
        }
        $question->save();
        return redirect()->route('questions')->with('message', 'Вопрос успешно обновлен.');
    }
}
