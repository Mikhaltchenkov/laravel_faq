<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ThreadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $threads = Thread::all();
        return view('threads.index', ['threads' => $threads]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:128|unique:threads',
        ]);

        $thread = new Thread;
        $thread->name = $request->name;
        $thread->created_by = Auth::user()->id;
        $thread->save();

        return redirect()->route('threads')->with('message', 'Тема успешно сохранена.');
    }

    public function destroy($id)
    {
        $thread = Thread::find($id);
        $thread->delete();

        return back()->with('message', 'Тема успешно удалена.');
    }

    public function edit($id)
    {
        $thread = Thread::find($id);
        return view('threads.edit', ['thread' => $thread]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:128',
        ]);

        $thread = Thread::find($id);
        $thread->update($request->all());
        $thread->save();
        return redirect()->route('threads')->with('message', 'Тема "' . $thread->name . '" успешно обновлена.');
    }
}
