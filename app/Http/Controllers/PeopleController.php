<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $peoples = People::all();
        return view('peoples.index', ['peoples' => $peoples]);
    }

    public function ban(Request $request, $id)
    {
        $people = People::find($id);
        $attribute['is_banned'] = ($people->is_banned == 0) ? 1 : 0;
        $people->update($attribute);
        $people->save();
        return redirect()->route('peoples')->with('message', 'Посетитель "' . $people->name . '" успешно обновлен.');
    }
}
