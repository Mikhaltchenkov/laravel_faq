<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $users = User::all();
        return view('users.index', ['users' => $users]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:64',
            'login' => 'required|max:64|unique:users',
            'password' => 'required|min:5',
        ]);

        User::create($request->all());

        return redirect()->route('users')->with('message', 'Пользователь успешно сохранен.');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return back()->with('message', 'Администратор успешно удален.');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', ['user' => $user]);
    }

    public function reset($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            back()->with('message', 'Пользователь с id ' . $id . ' не найден.');
        } else {
            return view('users.reset_password', ['user' => $user]);
        }
    }

    public function storePassword(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|max:64|min:5',
            'password_confirm' => 'required|max:64|min:5|same:password',
        ]);

        $user = User::find($id);
        $user->update($request->all());
        $user->save();
        return redirect()->route('users')->with('message', 'Пароль пользователя "' . $user->name . '" успешно обновлен.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:64',
            'login' => 'required|max:64',
        ]);

        $user = User::find($id);
        $user->update($request->all());
        $user->save();
        return redirect()->route('users')->with('message', 'Пользователь "' . $user->name . '" успешно обновлен.');
    }
}
