<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('people_id')->unsigned()->nullable();
            $table->text('question');
            $table->text('answer')->nullable();
            $table->smallInteger('state')->default('1');;
            $table->integer('thread_id')->unsigned()->nullable();
            $table->integer('answered_by')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('set null');
            $table->foreign('answered_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('thread_id')->references('id')->on('threads')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
