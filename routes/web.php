<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('front.index');
Route::get('/ask', 'FrontController@create')->name('front.create');
Route::post('/ask', 'FrontController@store')->name('front.store');


Auth::routes();

Route::group(['prefix'=>'admin'], function () {
    Route::get('/users', 'UserController@index')->name('users');
    Route::post('/users', 'UserController@store')->name('users.store');
    Route::get('/users/{user}', 'UserController@edit')->name('users.edit');
    Route::post('/users/{user}', 'UserController@update')->name('users.update');
    Route::delete('/users/{user}', 'UserController@destroy')->name('users.delete');
    Route::get('/users/{user}/reset', 'UserController@reset')->name('users.reset');
    Route::put('/users/{user}/reset', 'UserController@storePassword')->name('users.store_password');

    Route::get('/peoples', 'PeopleController@index')->name('peoples');
    Route::put('/peoples/{people}', 'PeopleController@ban')->name('peoples.ban');

    Route::get('/threads', 'ThreadController@index')->name('threads');
    Route::post('/threads', 'ThreadController@store')->name('threads.store');
    Route::get('/threads/{threads}', 'ThreadController@edit')->name('threads.edit');
    Route::post('/threads/{thread}', 'ThreadController@update')->name('threads.update');
    Route::delete('/threads/{thread}', 'ThreadController@destroy')->name('threads.delete');

    Route::get('/questions', 'QuestionController@index')->name('questions');
    Route::get('/questions/{question}', 'QuestionController@edit')->name('questions.edit');
    Route::post('/questions/{question}', 'QuestionController@update')->name('questions.update');
    Route::delete('/questions/{question}', 'QuestionController@destroy')->name('questions.delete');
    Route::put('/questions/{question}/unpublish', 'QuestionController@unpublish')->name('questions.unpublish');
    Route::put('/questions/{question}/publish', 'QuestionController@publish')->name('questions.publish');
});
